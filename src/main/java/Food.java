import java.awt.*;

/**
 * Created by Light on 19.11.2014.
 */
public class Food {

    private int x, y, width, height;

    public Food(int x, int y, int tileSize) {
        this.x = x;
        this.y = y;
        this.width = tileSize;
        this.height = tileSize;
    }

    public void draw(Graphics g) {
        g.setColor(Color.ORANGE);
        g.fillRect(x*width+1, y*height+1, width-1, height-1);
    }
}
