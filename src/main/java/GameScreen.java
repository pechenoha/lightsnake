import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Light on 19.11.2014.
 */
public class GameScreen extends JPanel implements Runnable {

    private static final int TILES_WIDTH = 40, TILES_HEIGHT = 30;
    public static final int tileSize = 20;
    private static final int WIDTH = TILES_WIDTH * tileSize, HEIGHT = TILES_HEIGHT * tileSize; // 800 x 400

    private static final int DELAY = 100;
    private Thread thread;
    private Key keyListener;
    private boolean running = false;


    private BodyPart b;
    private ArrayList<BodyPart> snake;
    private int snakeSize = 5;

    private int headX = 10, headY = 20;
    private Direction direction = Direction.RIGHT;
    private Food food;
    private Random r;

    // food
    int xFood;
    int yFood;

    public GameScreen() {
        setFocusable(true);
        keyListener = new Key();
        addKeyListener(keyListener);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        r = new Random();

        snake = new ArrayList<BodyPart>();

        start();
    }

    private void start() {
        running = true;
        thread =  new Thread(this, "Main Loop");
        thread.start();
    }

    private void stop() {
        running = false;
        JOptionPane.showMessageDialog(null, "Nice game :) \nYour score: " + snakeSize*10, "The end", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }

    private void tick() throws InterruptedException {
        // create snake
        if (snake.size() == 0) {
            for (int i = 0; i < snakeSize; i++) {
                b = new BodyPart(headX++, headY, tileSize);
                snake.add(b);
            }
            headX--;
        }
        // create food
        if (food == null) {
            xFood = r.nextInt(40);
            yFood = r.nextInt(30);
            food = new Food(xFood, yFood, tileSize);
        }
        // eat food
        if (xFood == headX && yFood == headY) {
            food = null;
            snakeSize++;
        }

        Thread.sleep(DELAY);
        // move snake's head - add one more body part at current coords and remove the last body part
        updateHeadCoords();
        b = new BodyPart(headX, headY, tileSize);
        snake.add(b);
        if (snake.size() > snakeSize) {
            snake.remove(0);
        }
        // THE END
        for (int i = 0; i < snake.size() - 1; i++) {
            if (headX == snake.get(i).getX() && headY == snake.get(i).getY()) {
                stop();
            }
        }
        if (headX < 0 || headX >= TILES_WIDTH|| headY < 0 || headY >= TILES_HEIGHT) {
            stop();
        }
    }

    private void updateHeadCoords() {
        if (direction == Direction.UP)      headY--;
        if (direction == Direction.DOWN)    headY++;
        if (direction == Direction.LEFT)    headX--;
        if (direction == Direction.RIGHT)   headX++;
    }

    public void paint(Graphics g) {
//        set background
//        g.setColor(Color.BLACK);
//        g.fillRect(0, 0, WIDTH, HEIGHT);
        // clear
        g.clearRect(0, 0, WIDTH, HEIGHT);
        // draw grid
        g.setColor(new Color(0xcccccc));
        for (int i = 0; i < WIDTH/20; i++) {
            g.drawLine(i*20, 0, i*20, HEIGHT);
        }
        for (int i = 0; i < HEIGHT/20; i++) {
            g.drawLine(0, i*20, WIDTH, i*20);
        }
        // draw snake's body parts
        for (int i = 0; i < snake.size(); i++) {
            snake.get(i).draw(g);
        }
        // draw food
        if (food != null) {
            food.draw(g);
        }
    }

    public void run() {
        try {
            while (running) {
                tick();
                repaint();
            }
        } catch (InterruptedException e) {}
    }

    private class Key implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_UP && direction != Direction.DOWN)
                direction = Direction.UP;
            else if (e.getKeyCode() == KeyEvent.VK_DOWN && direction != Direction.UP)
                direction = Direction.DOWN;
            else if (e.getKeyCode() == KeyEvent.VK_LEFT && direction != Direction.RIGHT)
                direction = Direction.LEFT;
            else if (e.getKeyCode() == KeyEvent.VK_RIGHT && direction != Direction.LEFT)
                direction = Direction.RIGHT;
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }
}
