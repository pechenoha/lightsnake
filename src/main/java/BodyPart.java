import java.awt.*;

/**
 * Created by Light on 19.11.2014.
 */
public class BodyPart {

    private int x;
    private int y;

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    private int width;
    private int height;

    public BodyPart(int x, int y, int tileSize) {
        this.x = x;
        this.y = y;
        this.width = tileSize;
        this.height = tileSize;
    }

    public void draw(Graphics g) {
        g.setColor(new Color(0x064B07));
        g.fillRect(x*width + 1, y*height + 1, width - 1, height - 1);
    }

}
