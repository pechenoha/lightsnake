import javax.swing.*;
import java.awt.*;

/**
 * Created by Light on 19.11.2014.
 */
public class App extends JFrame {

    public App() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("LightSnake v1.0");
        setResizable(false);
        init();
    }

    private void init() {
        setLayout(new GridLayout(1, 1, 0, 0));

        GameScreen screen = new GameScreen();
        add(screen);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static void main(String[] args) {
        App app = new App();
    }
}
